import xml.etree.ElementTree as ET
import os
import glob
import argparse
from preprocess import use_horizontal_flips, get_file_name, \
  use_vertical_flips, rotation_90, rotation_180, rotation_270, process_path

def get_args():
  parser = argparse.ArgumentParser()
  parser.add_argument(
      '--dataset', 
      help = "Enter the path to the train images dataset",
      default='../dataset/'
  )
  return parser




def create_datasets(path, image_path, folder_name):
  # get the annotation file list
  annotation_list = glob.glob(image_path + folder_name + "/*.xml")

  # List to stored labels
  label_names = []
  
  # Create paths 
  output_path =  path + 'output/' + folder_name
  try:
    os.makedirs(output_path)
  except:
    print("Already Exist")

  dataset_file = output_path+ '/' + 'data_annotation.txt'
  classes_file = output_path+ '/' + 'classes.txt'
  classes_file = open(classes_file, 'w')
  dataset_file = open(dataset_file, 'w')

  for ann in annotation_list:
    tree=ET.parse(ann)
    root = tree.getroot()
    for path in root.iter('path'):
      path = process_path(str(path.text))
      print("Processing Images: ",path)
      annot = ""
      for obj in root.iter('object'):
        for name in obj.iter('name'):
          label = str(name.text)

          if label not in label_names:
            label_names.append(label)
            classes_file.write(label+'\n')

        for bb in root.iter('bndbox'):
          for xmin in bb.iter('xmin'):
            x_min = str(xmin.text)
          for xmax in bb.iter('xmax'):
            x_max = str(xmax.text)
          for ymin in bb.iter('ymin'):
            y_min = str(ymin.text)
          for ymax in bb.iter('ymax'):
            y_max = str(ymax.text)
        annot += x_min+','+y_min+','+ x_max+ ','+y_max +','+ str(label_names.index(label))+' '
      _,file_name = get_file_name(path)
      dataset_file.write(file_name + ' ' + annot + '\n') 
      # Apply Augumentation and save image
      use_horizontal_flips(path,annot, dataset_file)
      use_vertical_flips(path,annot,dataset_file)
      rotation_90(path,annot,dataset_file)
      rotation_180(path,annot,dataset_file)
      rotation_270(path,annot,dataset_file)      
  classes_file.close()
  dataset_file.close()


def main():
  parser = get_args()
  args = parser.parse_args()
  dataset_train = args.dataset+'train_images/'
  for fl in os.listdir(dataset_train):
    print(dataset_train+fl)
    create_datasets(args.dataset, dataset_train, fl)


if __name__ == '__main__':
  main()