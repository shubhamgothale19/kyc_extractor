import sys
from PIL import Image
import shutil
import glob
import os
import cv2
import numpy as np
from preprocess import rotate_image, get_images, get_image_name, remove_backgroud


def main_process(images, out_path, dir_name, train_image_path, image_name, merge=False):
  for i in range(len(images)):
    new_im = np.array(images[i])
    new_im = remove_backgroud(new_im)
    new_im = Image.fromarray(new_im)
    new_im = new_im.resize(image_size)
    new_im.save(train_image_path+dir_name+'/'+image_name+'_'+str(i)+'.jpg')
  shutil.rmtree(out_path, ignore_errors=True)
  

def pdf2image(pdf_path, train_image_path):
  temp_path = ''
  dir_list = os.listdir(pdf_path)
  print(dir_list)
  for dir_name in dir_list:
    try:
      os.makedirs(train_image_path + dir_name)
    except:
      print("Already Exist")
    pdf_files = glob.glob(pdf_path + dir_name + "/*.pdf")
    for pdf_file in pdf_files:
      print("Processing ", pdf_file)
      image_name = get_image_name(pdf_file)
      images, out_path = get_images(pdf_file, temp_path)
      main_process(images, out_path, dir_name, train_image_path, image_name)
    image_files = glob.glob(pdf_path + dir_name + "/*.tif")
    for image_path in image_files:
      print("Processing ", image_path)
      image_name = get_image_name(image_path)
      image = Image.open(image_path)
      image = image.resize(image_size)
      image.save(train_image_path+dir_name+'/'+image_name+'_'+'.jpg')



pdf_path = "../dataset/pdf_data/"
train_image_path = "../dataset/train_images/"
image_size = (800,800)
pdf2image(pdf_path,train_image_path)
