import sys
from PIL import Image
import shutil
import glob
import os
import cv2
from skimage.transform import radon
from PIL import Image
from numpy import asarray, mean, array, blackman
import numpy as np
from numpy.fft import rfft
import matplotlib.pyplot as plt
try:
  from parabolic import parabolic
  def argmax(x):
    return parabolic(x, numpy.argmax(x))[0]
except ImportError:
  from numpy import argmax
from pdf2jpg import pdf2jpg
from PIL import Image

def rms_flat(a):
  """
  Return the root mean square of all the elements of *a*, flattened out.
  """
  return np.sqrt(np.mean(np.abs(a) ** 2))

def get_file_name(line):
  l = line.split('/')
  name = l[-1]
  path = l[:-1]
  name = name.replace(' ','')
  path = '/'.join(path)
  return path, name

def rotate_image(image):
  I = Image.fromarray(image)
  I = asarray(I.convert('L'))
  I = I - mean(I)  # Demean; make the brightness extend above and below zero
  
  # Do the radon transform and display the result
  sinogram = radon(I)

  # Find the RMS value of each row and find "busiest" rotation,
  # where the transform is lined up perfectly with the alternating dark
  # text and white lines
  r = array([rms_flat(line) for line in sinogram.transpose()])
  angle = argmax(r)
  angle = 90 - angle
  print('Rotation: {:.2f} degrees'.format(angle))

  # Rotate image to deskew
  (h,w) = image.shape[:2]
  center = (w//2 , h//2)
  M = cv2.getRotationMatrix2D(center, angle, 1.0)
  rotated = cv2.warpAffine(image, M, (w,h), flags=cv2.INTER_CUBIC, borderMode = cv2.BORDER_REPLICATE)
  return rotated

def get_images(input_path,output_path):
  # to convert all pages
  result = pdf2jpg.convert_pdf2jpg(input_path, output_path, pages="ALL")
  images = [Image.open(x) for x in result[0]['output_jpgfiles']]
  out_path = result[0]['output_pdfpath']
  return images, out_path


def get_image_name(path):
  name = path.split('/')[-1][:-4]
  name = name.replace(' ', '')
  return name


def remove_backgroud(img):
  ## (1) Convert to gray, and threshold
  gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
  th, threshed = cv2.threshold(gray, 240, 255, cv2.THRESH_BINARY_INV)
  
  ## (2) Morph-op to remove noise
  kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (100,100))
  morphed = cv2.morphologyEx(threshed, cv2.MORPH_CLOSE, kernel)

  ## (3) Find the max-area contour
  cnts = cv2.findContours(morphed, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
  cnt = sorted(cnts, key=cv2.contourArea)[-1]

  ## (4) Crop and save it
  x,y,w,h = cv2.boundingRect(cnt)
  dst = img[y:y+h, x:x+w]
  return dst

def get_boxes(annot):
  list_annot = annot.split(' ')[:-1]
  return list_annot

def save_image(img, path):
  image = Image.fromarray(img)
  image.save(path)

def use_horizontal_flips(file_path,annot,dataset_file):
  path,file_name = get_file_name(file_path)
  img = cv2.imread(file_path)
  rows, cols = img.shape[:2]
  img = cv2.flip(img, 1)
  bboxs = get_boxes(annot)
  hor_annot = ''
  for bb in bboxs:
    x_min, y_min, x_max, y_max, label = bb.split(',')
    x_min, y_min, x_max, y_max = int(x_min), int(y_min), int(x_max), int(y_max)
    x1 = x_min
    x2 = x_max
    x_max = cols - x1
    x_min = cols - x2
    hor_annot += str(x_min)+','+str(y_min)+','+ str(x_max)+ ','+str(y_max) +','+ str(label)+' '
  name = file_name[:-4]+"horizontal"+file_name[-4:]
  save_image(img, os.path.join(path,name))
  dataset_file.write(name + ' ' + hor_annot + '\n') 

def use_vertical_flips(file_path,annot,dataset_file):
  path,file_name = get_file_name(file_path)
  img = cv2.imread(file_path)
  rows, cols = img.shape[:2]
  img = cv2.flip(img, 0)
  bboxs = get_boxes(annot)
  ver_annot = ''
  for bb in bboxs:
    x_min, y_min, x_max, y_max, label = bb.split(',')
    x_min, y_min, x_max, y_max = int(x_min), int(y_min), int(x_max), int(y_max)
    y1 = y_min
    y2 = y_max
    y_max = rows - y1
    y_min = rows - y2
    ver_annot += str(x_min)+','+str(y_min)+','+ str(x_max)+ ','+str(y_max) +','+ str(label)+' '
  name = file_name[:-4]+"vertical"+file_name[-4:]
  save_image(img, os.path.join(path,name))
  dataset_file.write(name + ' ' + ver_annot + '\n') 


def rotation_90(file_path,annot,dataset_file):
  path,file_name = get_file_name(file_path)
  img = cv2.imread(file_path)
  rows, cols = img.shape[:2]
  img = np.transpose(img, (1,0,2))
  img = cv2.flip(img, 1)
  bboxs = get_boxes(annot)
  annot_90 = ''
  for bb in bboxs:
    x_min, y_min, x_max, y_max, label = bb.split(',')
    x_min, y_min, x_max, y_max = int(x_min), int(y_min), int(x_max), int(y_max)
    x1 = x_min
    x2 = x_max
    y1 = y_min
    y2 = y_max
    x_min = rows - y2
    x_max = rows - y1
    y_min = x1
    y_max = x2 
    annot_90 += str(x_min)+','+str(y_min)+','+ str(x_max)+ ','+str(y_max) +','+ str(label)+' '
  name = file_name[:-4]+"90_degree"+file_name[-4:]
  save_image(img, os.path.join(path,name))
  dataset_file.write(name + ' ' + annot_90 + '\n') 

def rotation_180(file_path,annot,dataset_file):
  path,file_name = get_file_name(file_path)
  img = cv2.imread(file_path)
  rows, cols = img.shape[:2]
  img = cv2.flip(img, -1)
  bboxs = get_boxes(annot)
  annot_180 = ''
  for bb in bboxs:
    x_min, y_min, x_max, y_max, label = bb.split(',')
    x_min, y_min, x_max, y_max = int(x_min), int(y_min), int(x_max), int(y_max)
    x1 = x_min
    x2 = x_max
    y1 = y_min
    y2 = y_max
    x_max = cols - x1
    x_min = cols - x2
    y_max = rows - y1
    y_min = rows - y2
    annot_180 += str(x_min)+','+str(y_min)+','+ str(x_max)+ ','+str(y_max) +','+ str(label)+' '
  name = file_name[:-4]+"180_degree"+file_name[-4:]
  save_image(img, os.path.join(path,name))
  dataset_file.write(name + ' ' + annot_180 + '\n') 

def rotation_270(file_path,annot,dataset_file):
  path,file_name = get_file_name(file_path)
  img = cv2.imread(file_path)
  rows, cols = img.shape[:2]
  img = np.transpose(img, (1,0,2))
  img = cv2.flip(img, 0)
  bboxs = get_boxes(annot)
  annot_270 = ''
  for bb in bboxs:
    x_min, y_min, x_max, y_max, label = bb.split(',')
    x_min, y_min, x_max, y_max = int(x_min), int(y_min), int(x_max), int(y_max)
    x1 = x_min
    x2 = x_max
    y1 = y_min
    y2 = y_max
    x_min = y1
    x_max = y2
    y_min = cols - x2
    y_max = cols - x1
    annot_270 += str(x_min)+','+str(y_min)+','+ str(x_max)+ ','+str(y_max) +','+ str(label)+' '
  name = file_name[:-4]+"270_degree"+file_name[-4:]
  save_image(img, os.path.join(path,name))
  dataset_file.write(name + ' ' + annot_270 + '\n') 

def process_path(path):
  start_idx = path.find('/dataset')
  path = '..'+path[start_idx:]
  return path