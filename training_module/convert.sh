python3 convert.py \
        --config_path "model_data/yolov3.cfg" \
        --weights_path "model_data/yolov3.weights" \
        --output_path "model_data/keras_yolov3.h5"