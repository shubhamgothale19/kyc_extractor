MAIN_PATH="../dataset"
DATA_PATH="output"
MODE="cheques"
SAVED_MODEL="saved_model"

IMAGE_PATH="$MAIN_PATH/train_images/$MODE/"
ANNOT_PATH="$MAIN_PATH/$DATA_PATH/$MODE/data_annotation.txt"
LOGS_DIR="$SAVED_MODEL/"
CLASSES_PATH="$MAIN_PATH/$DATA_PATH/$MODE/classes.txt"
ANCHOR_PATH="model_data/yolo_anchors.txt"
BATCH_SIZE=32
VALID_SPLIT=0.1
EPOCHS=30

python3 train.py \
        --image_path $IMAGE_PATH\
        --annotation_path $ANNOT_PATH\
        --dataset_name $MODE\
        --log_dir $LOGS_DIR\
        --classes_path $CLASSES_PATH\
        --anchors_path $ANCHOR_PATH\
        --batch_size $BATCH_SIZE\
        --epochs $EPOCHS\
        --valid_split $VALID_SPLIT